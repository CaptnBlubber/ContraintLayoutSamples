package de.arueggeberg.constraintlayoutworkshop.ui.positioning

import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import de.arueggeberg.constraintlayoutworkshop.R
import kotlinx.android.synthetic.main.fragment_anchored_positioning.*

class AnchoredPositionFragment : Fragment() {

    companion object {
        fun newInstance() = AnchoredPositionFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_anchored_positioning, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                //NOOP
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                //NOOP
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

                val newBias = progress / 100f


                val constraintSet = ConstraintSet()

                constraintSet.clone(anchoredConstraintLayout)
                constraintSet.setHorizontalBias(R.id.anchoredView, 0.5f)
                constraintSet.applyTo(anchoredConstraintLayout)

                ConstraintSet().run {
                    clone(anchoredConstraintLayout)
                    setHorizontalBias(R.id.anchoredView, newBias)
                    applyTo(anchoredConstraintLayout)
                }

                currentBias.text = "Horizontal Bias: $newBias"
            }

        })
    }
}

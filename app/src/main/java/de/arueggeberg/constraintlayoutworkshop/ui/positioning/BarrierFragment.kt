package de.arueggeberg.constraintlayoutworkshop.ui.positioning

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.arueggeberg.constraintlayoutworkshop.R
import kotlinx.android.synthetic.main.fragment_barriers.*

class BarrierFragment : Fragment() {

    companion object {
        fun newInstance() = BarrierFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_barriers, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.setOnClickListener {
            longText.visibility = View.GONE
        }
    }
}

package de.arueggeberg.constraintlayoutworkshop.ui.positioning

import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.arueggeberg.constraintlayoutworkshop.R
import kotlinx.android.synthetic.main.fragment_animated_start.*

class TransformingFragment : Fragment() {

    companion object {
        fun newInstance() = TransformingFragment()
    }

    var isExpanded = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_animated_start, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonTransform.setOnClickListener {
            ConstraintSet().run {
                clone(context!!, if (isExpanded) R.layout.fragment_animated_start else R.layout.fragment_animated_end)
                TransitionManager.beginDelayedTransition(transformingConstraint)
                applyTo(transformingConstraint)
            }

            isExpanded = !isExpanded
        }
    }
}

package de.arueggeberg.constraintlayoutworkshop.ui.positioning

import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.arueggeberg.constraintlayoutworkshop.R
import de.arueggeberg.constraintlayoutworkshop.ui.positioning.ChainsFragment.ChainState.PACKED
import de.arueggeberg.constraintlayoutworkshop.ui.positioning.ChainsFragment.ChainState.SPREAD
import de.arueggeberg.constraintlayoutworkshop.ui.positioning.ChainsFragment.ChainState.SPREAD_INSIDE
import kotlinx.android.synthetic.main.fragment_chains.*

class ChainsFragment : Fragment() {

    companion object {
        fun newInstance() = ChainsFragment()
    }

    var currentState = SPREAD

    enum class ChainState(val style: Int) {
        SPREAD(ConstraintSet.CHAIN_SPREAD),
        SPREAD_INSIDE(ConstraintSet.CHAIN_SPREAD_INSIDE),
        PACKED(ConstraintSet.CHAIN_PACKED)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_chains, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonToggle.text = currentState.name

        buttonToggle.setOnClickListener {
            currentState = when (currentState) {

                SPREAD -> SPREAD_INSIDE
                SPREAD_INSIDE -> PACKED
                PACKED -> SPREAD
            }

            buttonToggle.text = currentState.name

            ConstraintSet().run {
                clone(constraintChains)
                setHorizontalChainStyle(textView1.id, currentState.style)
                applyTo(constraintChains)
            }

        }

    }
}

package de.arueggeberg.constraintlayoutworkshop.ui.positioning

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.arueggeberg.constraintlayoutworkshop.R

class ContainerPositioningFragment : Fragment() {

    companion object {
        fun newInstance() = ContainerPositioningFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_container_positioning, container, false)


}
